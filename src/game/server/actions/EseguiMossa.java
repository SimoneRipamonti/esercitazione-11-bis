package game.server.actions;

import game.server.model.GameState;
import game.server.model.Mossa;
import game.server.model.Partita;
import game.server.model.Player;

import java.util.Iterator;
import java.util.Set;

public class EseguiMossa extends Action {

	private final Mossa mossa;

	public EseguiMossa(Mossa mossa, Player player) {
		super(player);
		this.mossa = mossa;

	}

	@Override
	public void esegui(Partita model) {

		/**
		 * prendo il lock sul modello, cosi non si hanno modifiche concorrenti
		 * alla stessa partita. se arrivano due azioni in contepornea viene
		 * prima eseguita una e poi l'altra, cosi si rispetta l'integrita
		 * 
		 */
		System.out.println("SERVER: performing ");
		synchronized (model) {
			System.out.println(model.getGameState());
			if (((model.getGameState().equals(GameState.RUNNING) || model
					.getGameState().equals(GameState.ONE_MOSSA_RECEIVED)) && !model
					.hasPerformedAnAction(this.getPlayer()))) {
				/**
				 * computo la mossa
				 */
				model.addMossa(this.getPlayer(), this.mossa);
				/**
				 * cambio lo stato
				 */
				System.out.println("Updating the game state");
				GameState gameState = model.getGameState();
				model.setGameState(gameState.nextState());
				/**
				 * se lo stato � ended gestisco chi ha vinto
				 */

				if (model.getGameState() == GameState.ENDED) {
					/**
					 * ottengo i due giocatori
					 */
					Set<Player> players = model.getPlayers();
					Iterator<Player> it = players.iterator();

					Player player1 = it.next();
					Player player2 = it.next();

					/**
					 * vedo se la mossa del giocatore 1 batte la mossa del
					 * giocatore 2
					 */
					if (model.getPlayersActions().get(player1)
							.batte(model.getPlayersActions().get(player2))) {
						model.setWinner(player1);
					} else {
						model.setWinner(player2);
					}
				}
			}

		}

	}
}
