package game.server.actions;

import game.server.model.Partita;
import game.server.model.Player;

public abstract class Action{

	private Player player;

	public Action(Player player) {
		this.setPlayer(player);
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	public abstract void esegui(Partita model);
}
