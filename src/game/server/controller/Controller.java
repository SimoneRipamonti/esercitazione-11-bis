package game.server.controller;

import game.server.actions.Action;
import game.server.model.Partita;

import java.util.Observable;
import java.util.Observer;

/**
 * contains the controller of the game
 * 
 * @author Claudio Menghi
 * 
 */
public class Controller implements Observer{

	
	private Partita partita;

	/**
	 * creates a new Controller
	 */
	public Controller(Partita partita) {
		this.partita=partita;
	}

	
	@Override
	public void update(Observable o, Object arg) {
		Action action=(Action) arg;
		System.out.println("Server: the thread that is performing the action have been lanched ");
		
		action.esegui(partita);
	}
}
