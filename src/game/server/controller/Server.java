package game.server.controller;

import game.server.model.GameState;
import game.server.model.Partita;
import game.server.view.ServerSocketView;
import game.server.view.View;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	private final static int SOCKET_PORT = 29999;

	private Partita partita;
	private Controller controller;

	/**
	 * Attendo che la partita parta per entrare in attesa di una nuova partita
	 */

	public Server() {
		partita = new Partita();
		controller = new Controller(partita);
	}

	public void start() throws IOException {
		/**
		 * avvio socket, e anche rmi (non ora)
		 */
		this.startSocket();
	}

	private void startSocket() throws IOException {
		ExecutorService executor = Executors.newCachedThreadPool();
		ServerSocket serverSocket = new ServerSocket(SOCKET_PORT);
		System.out.println("SERVER: ready on the port " + SOCKET_PORT);

		while (true) {
			/**
			 * in attesa di connesioni all'infinito
			 */
			Socket socket = serverSocket.accept();
			/**
			 * creo una view per quel socket
			 */
			ServerSocketView view = new ServerSocketView(socket);

			/**
			 * metto il client in ascolto sulla view
			 */
			this.addClient(view, partita, controller);
			
			executor.submit(view);
		}
	}

	/**
	 * Non serve synchronized perch� per accettare un nuovo giocatore il server
	 * deve ritornare ad accept() e questo avviene dopo aver eseguito tutta
	 * addClient quindi non si pu� verificare che tre giocatori si connettano
	 * alla stessa partita
	 * 
	 * @param view
	 * @param partita
	 * @param controller
	 */
	public void addClient(View view, Partita partita, Controller controller) {
		/**
		 * Aggiungo alla view il controller come osservatore
		 */
		view.addObserver(controller);
		/**
		 * Aggiungo alla partita l'osservatore view
		 */
		partita.addObserver(view);
		/**
		 * aggiorno lo stato della partita
		 */
		partita.setGameState(partita.getGameState().nextState());
		/**
		 * se la partita � piena, creo una nuova partita per altri client, se
		 * non c'� una partita piena non posso creane di nuove, i giocatori sono
		 * obbligati a riempirla. posso modificare la logica di gestione delle
		 * partite a piacimento, posso far quello che voglio.
		 */
		if (partita.getGameState().equals(GameState.RUNNING)) {
			partita = new Partita();
			controller = new Controller(partita);
		}
	}
	
	public static void main (String [] args) throws IOException{
		Server server = new Server();
		server.start();
	}
}
