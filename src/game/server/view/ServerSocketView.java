package game.server.view;

import game.server.actions.Action;
import game.server.actions.EseguiMossa;
import game.server.model.Mossa;
import game.server.model.Player;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Observable;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ServerSocketView extends View implements Runnable {

	private final Socket socket;
	private Scanner socketIn;
	private PrintWriter socketOut;

	public ServerSocketView(Socket socket) throws IOException {
		this.socket = socket;
		socketIn = new Scanner(socket.getInputStream());
		socketOut = new PrintWriter(socket.getOutputStream());
	}

	@Override
	public void update(Observable o, Object arg) {
		/**
		 * Update viene chiamata quando c'� un cambiamento nel modello
		 */
		
		System.out.println("SERVER: sending the message to the client");
		socketOut.println((String)arg);
		socketOut.flush();

	}

	@Override
	public void run() {

		while (true) {
			String line = socketIn.nextLine();
			System.out.println("SERVER: getting the command " + line);
			/**
			 * A questo punto bisogna: (1) o definire tutti i comandi stringa
			 * che client e server possono scambiarsi (2) o inviarsi oggetti
			 * tramite serializzazione (3) o inviarsi json, xml, ...
			 * 
			 * in questo ci aspettiamo comandi del tipo "player1 mossa"
			 * 
			 */
			/**
			 * Divide una stringa in token, di default con lo spazio
			 */
			StringTokenizer tokenizer = new StringTokenizer(line);
			/**
			 * Reimplemntato hashcode e equals, due player con lo stesso nome
			 * sono due giocatori uguali, idem per le mosse
			 */
			Player player = new Player(tokenizer.nextToken());

			Mossa mossa = Mossa.valueOf(tokenizer.nextToken().toUpperCase());

			System.out.println("SERVER: " + mossa);
			
			Action action = new EseguiMossa(mossa, player);
			/**
			 * La mossa viene ascoltata dal controllore, che � in ascolto sui relativi socket
			 */
			this.setChanged();
			this.notifyObservers(action);
		}

	}

}
