package game.server.model;


/**
 * contains the different actions the user can perform
 * @author Claudio Menghi
 *
 */
public enum Mossa{	
	
	FORBICE  {
		@Override
		public boolean batte(Mossa other) {
			
			return other == CARTA;
		}
	},	
	CARTA {
		@Override
		public boolean batte(Mossa other){
			return other == PIETRA;
		}
	},
	PIETRA { 
		@Override
		public boolean batte(Mossa other){
			return other == FORBICE;
		} 
	};
	
	public abstract boolean batte(Mossa other);
}