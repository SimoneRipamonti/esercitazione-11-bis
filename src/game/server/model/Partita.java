package game.server.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

/**
 * contains the model of the game
 * 
 * @author Claudio Menghi
 * 
 */
public class Partita extends Observable{

	// contains the number of player of the game
	private static final int nPlayers = 2;
	
	private Player winner;
	
	/**
	 * mappa che contiene per ogni giocatore la mossa scelta
	 */
	private Map<Player, Mossa> mossa;

	// contains the current state of the game
	private GameState gameState;

	public Partita() {
		this.gameState = GameState.WAITINGTWOPLAYERS;
		this.mossa = new HashMap<Player, Mossa>();
		this.mossa.put(new Player("player1"), null);
		this.mossa.put(new Player("player2"), null);
	}

	public void addMossa(Player player, Mossa mossa){
		this.mossa.put(player, mossa);
	}
	
	/**
	 * returns the number of players
	 * 
	 * @return the number of players
	 */
	public static int getNplayers() {
		return nPlayers;
	}

	/**
	 * returns the state of the game
	 * 
	 * @return the current state of the game
	 */
	public GameState getGameState() {
		return gameState;
	}

	/**
	 * sets the state of the game
	 * 
	 * @param gameState
	 *            sets the state of the game to the gameState
	 */
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
		this.setChanged();
		this.notifyObservers("Game state: "+gameState);
	}
	
	/**
	 * ritorna l'insieme dei giocatori
	 * @return the set of the players
	 */
	public Set<Player> getPlayers(){
		return this.mossa.keySet();
	}
	
	public boolean hasPerformedAnAction(Player player){
		return this.mossa.get(player)!=null;
	}
	
	
	public Map<Player, Mossa> getPlayersActions(){
		return Collections.unmodifiableMap(this.mossa);
	}



	public Player getWinner() {
		return winner;
	}

	public void setWinner(Player winner) {
		
		this.winner = winner;
		this.setChanged();
		this.notifyObservers("The winner is "+winner);
	}

}