package game.server.model;


/**
 * contains the possible states of the game
 * @author Claudio Menghi
 *
 */
public enum GameState {	
	
	WAITINGTWOPLAYERS  {
		@Override
		public GameState nextState() {
			
			return WAITINGONEPLAYER;
		}
	},
	WAITINGONEPLAYER  {
		@Override
		public GameState nextState() {
			
			return RUNNING;
		}
	},
	RUNNING  {
		@Override
		public GameState nextState() {
			
			return ONE_MOSSA_RECEIVED;
		}
	},
	ONE_MOSSA_RECEIVED{
		@Override
		public GameState nextState() {
			
			return ENDED;
		}
	},
	ENDED  {
		@Override
		public GameState nextState() {
			
			return ENDED;
		}
	};
	
	public abstract GameState nextState();
}